import {Controller} from 'tramway-core';

export default class SecuredController extends Controller {
    constructor(){
        super();
    }

    static index (req, res) {
        return res.send(`Page rendered securly`);
    }
}