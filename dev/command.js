import {commands} from 'tramway-command';
let {CommandResolver} = commands;
import commandsIndex from './config/commands';

export default (new CommandResolver(commandsIndex)).run();